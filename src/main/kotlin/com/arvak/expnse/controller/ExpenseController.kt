package com.arvak.expnse.controller

import com.arvak.expnse.model.Expense
import com.arvak.expnse.service.ExpenseService
import kotlinx.coroutines.flow.Flow
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class ExpenseController {

    @Autowired
    private lateinit var expenseService: ExpenseService

    @GetMapping("/api/v1/expenses")
    suspend fun getExpenses(): Flow<Expense> = expenseService.getExpenses()

}