package com.arvak.expnse.service

import com.arvak.expnse.model.Expense
import com.arvak.expnse.repository.ExpenseRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.reactive.asFlow
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ExpenseService {

    @Autowired
    private lateinit var expenseRepository: ExpenseRepository

    fun getExpenses(): Flow<Expense> = expenseRepository.findAll().asFlow()

}