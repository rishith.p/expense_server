package com.arvak.expnse.model

import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.MongoId
import java.util.Date

@Document
data class Expense(

    @Id
    val id: String? = null,

    val name: String,

    val category: String,

    val price: Double,

    val date: Date
)