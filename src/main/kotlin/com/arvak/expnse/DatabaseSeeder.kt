package com.arvak.expnse

import com.arvak.expnse.model.Expense
import com.arvak.expnse.repository.ExpenseRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component
import java.util.*

@Component
class DatabaseSeeder: CommandLineRunner {

    @Autowired
    private lateinit var expenseRepository: ExpenseRepository

    override fun run(vararg args: String?) {
        val expense1 = Expense(
            name = "Panner",
            category = "Groceries",
            date = Date(),
            price = 80.0
        )

        val expense2 = Expense(
            name = "Oil",
            category = "Groceries",
            date = Date(),
            price = 140.0
        )
        expenseRepository.deleteAll().block()

        expenseRepository.save(expense1).block()
        expenseRepository.save(expense2).block()
    }
}