package com.arvak.expnse.repository

import com.arvak.expnse.model.Expense
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository

interface ExpenseRepository: ReactiveMongoRepository<Expense, String>