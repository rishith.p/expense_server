package com.arvak.expnse.configuration

import com.arvak.expnse.repository.ExpenseRepository
import com.mongodb.reactivestreams.client.MongoClient
import com.mongodb.reactivestreams.client.MongoClients
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.config.AbstractReactiveMongoConfiguration
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories

@Configuration
@EnableReactiveMongoRepositories(
    basePackageClasses = [ExpenseRepository::class]
)
class MongoConfiguration: AbstractReactiveMongoConfiguration() {

    @Value("\${database.mongodb.name}")
    private lateinit var databaseName: String

    @Value("\${database.mongodb.host}")
    private lateinit var databaseHost: String

    @Value("\${database.mongodb.port}")
    private lateinit var databasePort: String

    override fun getDatabaseName(): String = databaseName

    override fun reactiveMongoClient(): MongoClient = mongoClient()

    @Bean
    fun mongoClient(): MongoClient = MongoClients.create("$databaseHost:$databasePort")

    @Bean
    fun reactiveMongoTemplate(): ReactiveMongoTemplate = ReactiveMongoTemplate(mongoClient(), databaseName)
}