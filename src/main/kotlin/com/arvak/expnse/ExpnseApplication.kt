package com.arvak.expnse

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories

@SpringBootApplication
class ExpnseApplication

fun main(args: Array<String>) {
    runApplication<ExpnseApplication>(*args)
}